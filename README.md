# Plugin HAL

![HAL](./prive/themes/spip/images/hal_m-xx.svg)

Le plugin HAL permet d'afficher les publications du portail [HAL](https://hal.science/) à travers un modèle SPIP unique ```<hal|param1|param2|...>```


## Le modèle ```<hal>```

Tous les paramètres sont facultatifs.  Si plusieurs paramètres sont indiqués, seules les références remplissant TOUTES les conditions seront sélectionnées.


| Paramètre |  Explications  | Exemple |
| -------- | -------- |-------- |
| q    |  Requête de recherche  sur l'API HAL https://api.archives-ouvertes.fr/docs/search   | `<hal\|q=Mali>` |
| collection     | Restreindre à une collection     |`<hal\|collection=CEPED>`|
| auteur     | Restreindre à un auteur - fournir `idHal`     |`<hal\|auteur=joseph-larmarange>`|
| max    | Nombre de résultats affiché de 1 à 10000. Valeur par défaut définie dans le panel de configuration du plugin sinon 10     |`<hal\|max=4>`|
| max_auteurs | Restreindre le nombre d'auteurs/autrices affiché de 1 à 1000. Si la liste est restreinte. `et al.` est affiché. Valeur par défaut définie dans le panel de configuration du plugin sinon 1000.   |`<hal\|max_auteurs=3>`|
| tri     | Indique le tri de requete. Par défaut pertinence https://api.archives-ouvertes.fr/docs/search/?#sort    | `<hal\|tri=submittedDate_tdate desc>`|
| debug     | Affiche la requête vers l'API HAL pour aider au déboguage    |`<hal\|debug=oui>`|
| dernieres_publications     | Via class, si renseigné, on force le tri sur le critère `publicationDate_tdate`    |`<hal\|dernieres_publications>`|
| masquer_lien    | Valable uniquement si `dernieres_publications` est renseignée. si renseigné, on n'affiche pas les liens *voir toutes publications* après la liste des résultats   |`<hal\|dernieres_publications\|auteur=joseph-larmarange\|masquer_lien=oui>` |
| halid   | Retourne le document unique correspondant au `HALid`   |`<hal\|halid=ird-03950125>`|
| doi	   | Retourne le document unique correspondant au `doi` |`<hal\|doi=10.1371/journal.pone.0280479>`|



## Documentation de l'API HAL
https://api.archives-ouvertes.fr/docs/search

## Documentation du plugin SPIP
https://contrib.spip.net/5478
