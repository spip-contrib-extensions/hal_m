<?php
/**
 * Utilisations de pipelines par Publications HAL
 *
 * @plugin     Publications HAL
 * @copyright  2016-2023
 * @author     erational
 * @licence    GNU/GPL
 * @package    SPIP\hal_m\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION'))  {
	return;
}	

/**
 * Ajout CSS de HAL_M
 *
 * @pipeline insert_head
 * @param  string $flux Contenu du head
 * @return string Contenu du head
 */
function hal_m_insert_head_css($flux) {
	$flux .= "\n<link rel='stylesheet' type='text/css' media='all' href='".find_in_path("css/hal_m.css")."' />\n";
	return $flux;
}

function hal_m_insert_head_prive_css($flux) {
	return hal_m_insert_head_css($flux);
}


